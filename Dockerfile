FROM rust:alpine AS builder
RUN apk add musl-dev
WORKDIR /build
COPY . .
RUN cargo build --release

FROM alpine
WORKDIR /tools
COPY --from=builder /build/target/release/mqtt_health .
