use rumqttc::{Client, MqttOptions, QoS};
use serde::Deserialize;
use std::time::Duration;

const STARTUP_SECS: u64 = 1;
const TIMEOUT_SECS: u64 = 2;
const PAYLOAD: [u8; 5] = [42, 14, 50, 55, 0];

#[derive(Deserialize)]
struct Configuration {
    broker_addr: String,
    ping_topic: String,
    echo_topic: String,
}
impl Configuration {
    fn from_file() -> Self {
        let path = std::env::var("CONFIG_PATH").expect("Provide CONFIG_PATH");
        serde_json::from_str(&std::fs::read_to_string(path).expect("Configuration file not found"))
            .unwrap()
    }
    fn from_env() -> Result<Self, std::env::VarError> {
        Ok(Self {
            broker_addr: std::env::var("BROKER")?,
            ping_topic: std::env::var("HEALTH_CHECK_PING")?,
            echo_topic: std::env::var("HEALTH_CHECK_ECHO")?,
        })
    }
    fn load() -> Self {
        if let Ok(res) = Self::from_env() {
            res
        } else {
            Self::from_file()
        }
    }
}
fn main() -> Result<(), ()> {
    let config = Configuration::load();
    let mut mqttoptions =
        MqttOptions::new(uuid::Uuid::new_v4().to_string(), config.broker_addr, 1883);
    mqttoptions.set_keep_alive(Duration::from_secs(5));

    let (mut client, mut connection) = Client::new(mqttoptions, 10);
    client
        .subscribe(config.echo_topic, QoS::AtMostOnce)
        .unwrap();
    std::thread::spawn(move || {
        std::thread::sleep(std::time::Duration::from_secs(STARTUP_SECS));
        client
            .publish(config.ping_topic, QoS::AtLeastOnce, false, PAYLOAD)
            .unwrap();
    });

    let start = std::time::Instant::now();
    // Iterate to poll the eventloop for connection progress
    for notification in connection.iter() {
        if let Ok(ev) = notification {
            match ev {
                rumqttc::Event::Incoming(ev) => match ev {
                    rumqttc::Packet::Publish(msg) => {
                        if msg.payload.to_vec() == PAYLOAD {
                            return Ok(());
                        }
                    }
                    _ => (),
                },
                rumqttc::Event::Outgoing(_) => (),
            }
        }
        if std::time::Instant::now().duration_since(start).as_secs() > TIMEOUT_SECS + STARTUP_SECS {
            return Err(());
        }
    }
    Err(())
}
